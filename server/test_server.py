import unittest
import requests
import json

class TestServer(unittest.TestCase):
	SITE_URL = 'http://localhost:51054'
	print('Testing for Server: ' + SITE_URL)
	RECIPE_URL = SITE_URL + '/recipe/'
	MESSAGE_URL = SITE_URL + '/board/'

	
	def test_post_recipe(self):
		recipe = {}

		recipe['foodName'] = 'Chicken'
		recipe['diet'] = ''
		recipe['intolerances'] = ''
		recipe['cuisine'] = 'italian'
		recipe['number'] = 2

		r = requests.post(self.RECIPE_URL, data = json.dumps(recipe))
		print(r.content)
		resp = json.loads(r.content.decode('utf-8'))

		self.assertEqual("success", resp['result'])
	'''	
	
	def test_get_recipe(self):
		recipe = {}

		recipe['foodName'] = 'Chicken'
		recipe['diet'] = ''
		recipe['intolerances'] = ''
		recipe['cuisine'] = 'italian'
		recipe['number'] = 2

		resp = requests.post(self.RECIPE_URL, data = json.dumps(recipe))
		response = json.loads(resp.content.decode('utf-8'))
		
		response2 = requests.get(self.RECIPE_URL + str(response['id'][0]))
		response2 = json.loads(response2.content.decodee('utf-8'))
		self.assertEqual("success", response2['result'])
	'''

if __name__ == '__main__':
	unittest.main()
