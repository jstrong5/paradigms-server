import cherrypy
from recipeController import RecipeController
from recipe_library import _recipe_database
from messageBoardController import MessageBoardController
from messageboard_library import _message_board

class optionsController:
	def OPTIONS(self, *args, **kwargs):
		return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

def start_service():
	dispatcher = cherrypy.dispatch.RoutesDispatcher()

	recipeController = RecipeController()

	#dispatchers
	dispatcher.connect('recipe_post', '/recipe/', controller=recipeController, action='POST_RECIPE', conditions=dict(method=['POST']))
	dispatcher.connect('recipe_get', '/recipe/:recipe_id', controller=recipeController, action='GET_RECIPE',conditions=dict(method=['GET']))
	dispatcher.connect('recipe_post_board', '/board/', controller=MessageBoardController, action='POST_RECIPE_BOARD',conditions=dict(method=['POST']))


	#cors dispatchers
	dispatcher.connect('recipe_post_options', '/recipe/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('recipe_get_options', '/recipe/:recipe_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('recipe_post_board_options', '/board/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

	conf = {
					 'global': {
									 'server.socket_host': 'localhost',
									 'server.socket_port': 51054,
						},
					 '/': {
									 	'request.dispatch': dispatcher,
										'tools.CORS.on':True,
						}
					}

	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)
#end of start service

if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
	start_service()
