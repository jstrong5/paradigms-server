import cherrypy
import json
import pprint
from recipe_library import _recipe_database

class RecipeController():
	def __init__(self):
		self.recipe = _recipe_database()

	def POST_RECIPE(self):
		''' loads in recipe from API and appends it to our list and displays information to user'''
		''' returns a list of the recipe ID's that we found'''
		output = {"result":"success"}

		data = json.loads(cherrypy.request.body.read().decode('utf-8'))

		try:
			output['id'] = self.recipe.load_recipe(data['foodName'], data['diet'], data['intolerances'], data['cuisine'], data['number'])
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = ex

		return json.dumps(output)

	def GET_RECIPE(self, recipe_id):
		'''return recipe from database'''
		output = {"result": "success"}
		recipe_id = int(recipe_id)

		try:
			response = self.recipe.getRecipe(recipe_id)
			response = json.loads(response)
			output['recipe'] = response
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = ex

		return json.dumps(output)
