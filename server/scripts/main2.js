console.log("Page Loaded")

var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = addRecipe;

function addRecipe() {
  console.log("Entered Message Board");
  var title = document.getElementById('foodTitle').value;
  var ingredients = document.getElementById('foodIngredients').value;
  var instructions = document.getElementById('foodInstructions').value;
  var url = document.getElementById('url').value;

  console.log("title: " + title + " " + "ingredients: " + ingredients + " " + "instructions: " + instructions);

  recipe_dict = {}
  recipe_dict['title'] = title;
  recipe_dict['ingredients'] = ingredients;
  recipe_dict['instructions'] = instructions;
  recipe_dict['url'] = url;

  // add to paragraph
  var para_item = document.createElement("p");
	para_item.setAttribute("id", "dynamic-para");
  para_item.style.border = "thin solid black";
  para_item.style.fontFamily = "sans-serif";

  // add Title header
  var para1 = document.createElement("H1");
  para1.style.fontFamily = "sans-serif";
  para1.appendChild(document.createTextNode(recipe_dict['title']))
  para1.appendChild(document.createElement("br"));
  para_item.appendChild(para1);

  // add image from URL
  var image_item = document.createElement("img");
  image_item.src = recipe_dict['url'];
  image_item.style.border = "thin solid black";
  para_item.appendChild(image_item);
  para_item.appendChild(document.createElement("br"));
  para_item.appendChild(document.createElement("br"));

  // add Ingredients header
  var para2 = document.createElement("p");
  para2.style.fontFamily = "sans-serif";
  para2.style.fontSize = "large";
  para2.appendChild(document.createTextNode("Ingredients: "));
  para2.appendChild(document.createElement("br"));
  para_item.appendChild(para2);

  // add Ingredients
  var para3 = document.createElement("p");
  para3.style.fontFamily = "sans-serif";
  para3.style.fontSize = "medium";
  para3.appendChild(document.createTextNode(recipe_dict['ingredients']));
  para3.appendChild(document.createElement("br"));
  para_item.appendChild(para3);

  // add Instructions header
  var para4 = document.createElement("p");
  para4.style.fontFamily = "sans-serif";
  para4.style.fontSize = "large";
  para4.appendChild(document.createTextNode("Instructions: "));
  para4.appendChild(document.createElement("br"));
  para_item.appendChild(para4);

  // add instructions
  var para5 = document.createElement("p");
  para5.style.fontFamily = "sans-serif";
  para5.style.fontSize = "medium";
  para5.appendChild(document.createTextNode(recipe_dict['instructions']));
  para5.appendChild(document.createElement("br"));
  para_item.appendChild(para5);

  var htmlResp = document.getElementById("response-div2");
  htmlResp.prepend(para_item);

  // http request
  var xhr = new XMLHttpRequest();
  var url = "http://localhost:51054/board/";

  console.log("URL: " + url);
  xhr.open("POST", url, true);

  xhr.onload = function(e) {
    var response = JSON.parse(xhr.responseText);
    console.log(response);
    //displayMessage(response);
  }

  xhr.onerror = function(e) {
    console.error(xhr.statusText);
  }

  xhr.send(JSON.stringify(recipe_dict));
}
