console.log("Page Loaded")

var searchButton = document.getElementById('search-button');
searchButton.onmouseup = getFormInfo;

var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearScreen;

function clearScreen() {
  document.getElementById("response-div").innerHTML = "";
}

function getFormInfo() {
  console.log("Retrieving Form Info")

  form_dict = {}

  // get form input
  var foodName  = document.getElementById('foodName').value;
  var cuisine   = document.getElementById('cuisine').value;
  var diet      = document.getElementById('diet').value;
  var intolerances  = document.getElementById('intolerances').value;
  var number = document.getElementById('numRecipe').value;

  form_dict['foodName'] = foodName;
  form_dict['cuisine']  = cuisine;
  form_dict['diet']     = diet;
  form_dict['intolerances'] = intolerances;
  form_dict['number']= number;

  console.log(form_dict);

  serverRequestPOST(form_dict);
}

function serverRequestPOST(form_dict) {
  console.log("In serverRequestPOST");

  var xhr = new XMLHttpRequest();
  var url = "http://localhost:51054/recipe/";

  console.log("URL: " + url);
  xhr.open("POST", url, true);

  xhr.onload = function(e) {
		var response = JSON.parse(xhr.responseText);
		console.log("Response from POST: " + response)

		for (i = 0; i < response['id'].length; i++) {
    	serverRequestGET(response['id'][i]);
		}
	}

  xhr.onerror = function(e) {
    console.error(xhr.statusText);
    // add to end of html doc
    var para = document.createElement("p");
    para.appendChild(document.createTextNode("Could not process your request. Try again"));
    var htmlResp = document.getElementById("response-div");
    htmlResp.prepend(para);
  }

  xhr.send(JSON.stringify(form_dict));

}

function serverRequestGET(id) {
	console.log("In serverRequestGET");
	console.log("ID: " + id);

  var xhr = new XMLHttpRequest();
  var url = "http://localhost:51054/recipe/" + id;

  console.log("URL: " + url);
  xhr.open("GET", url, true);

	xhr.onload = function(e) {
		var response = JSON.parse(xhr.responseText);
    console.log(response);
		display(response);
	}

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}

	xhr.send(null);
}

function display(response) {
	var para_item = document.createElement("p");
  para_item.style.border = "thin solid black";
  para_item.style.margin = "5%";

  // add recipe title
  var header = document.createElement('H1');
  header.style.fontFamily = "sans-serif";
  header.appendChild(document.createTextNode(response['recipe']['recipeTitle']));
  para_item.appendChild(header);
  para_item.appendChild(document.createElement("br"));


  var para1 = document.createElement("p");
  para1.style.fontFamily = "sans-serif";
  para1.style.fontSize = "medium";

  // append health score
	para1.appendChild(document.createTextNode("Health Score: " + response['recipe']['healthScore']));
  para1.appendChild(document.createElement("br"));
  // append rating
  para1.appendChild(document.createTextNode("Rating: " + response['recipe']['rating']));
  para1.appendChild(document.createElement("br"));
  // append servings_node
  para1.appendChild(document.createTextNode("Servings: " + response['recipe']['servings']));
  para1.appendChild(document.createElement("br"));
  // append prepTime
  para1.appendChild(document.createTextNode("Preparation Time: " + response['recipe']['prepTime']));
  para1.appendChild(document.createElement("br"));
  para_item.appendChild(para1);

  // add blank space
  para_item.appendChild(document.createElement("br"));

  // add image
  var image_item = document.createElement("img");
  image_item.src = response['recipe']['imageURL'];
  image_item.style.border = "thin solid black";
  image_item.style.marginLeft = "10%";
  para_item.appendChild(image_item);
  para_item.appendChild(document.createElement("br"));
  para_item.appendChild(document.createElement("br"));

  // dispaly ingredients header
  var para2 = document.createElement("p");
  para2.style.fontFamily = "sans-serif";
  para2.style.fontSize = "large";
  para2.appendChild(document.createTextNode("Ingredients: "));
  para2.appendChild(document.createElement("br"));
  para_item.appendChild(para2);

  // display recipe ingredients
  var para3 = document.createElement("p");
  para3.style.fontFamily = "sans-serif";
  for (i = 0; i < response['recipe']['recipeIngredients'].length; i++) {
    para3.appendChild(document.createTextNode(response['recipe']['recipeIngredients'][i]));
    para3.appendChild(document.createElement("br"));
  }
  para_item.appendChild(para3);

  // display instructions header
  var para4 = document.createElement("p");
  para4.style.fontFamily = "sans-serif";
  para4.style.fontSize = "large";
  para4.appendChild(document.createTextNode("Instructions: "));
  para4.appendChild(document.createElement("br"));
  para_item.appendChild(para4);

  // add instructions
  var para5 = document.createElement("p");
  para5.style.fontFamily = "sans-serif";
  for (i = 0; i < response['recipe']['recipeInstructions'].length; i++) {
    para5.appendChild(document.createTextNode((i + 1) + ". " + response['recipe']['recipeInstructions'][i]));
    para5.appendChild(document.createElement("br"));
  }
  para_item.appendChild(para5);

  // add to end of html doc
  var htmlResp = document.getElementById("response-div");
  htmlResp.prepend(para_item);
}
