
class _message_board:
	def __init__(self):
		self.recipe = list()

	def add_recipe(self, title="", ingredients="", instructions="", url=""):
		recipe = {}
		recipe['title'] = title
		recipe['ingredients'] = ingredients
		recipe['instructions'] = instructions
		recipe['url'] = url
		self.recipe.append(recipe)


if __name__ == '__main__':
	board = _message_board()
