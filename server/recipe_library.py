import json
import requests
import pprint

class _recipe_database:
		def __init__(self):
			self.rid = list()
			self.recipeTitle = list()
			self.recipeIngredients = list()
			self.recipeInstructions = list()
			self.equipment = list()
			self.imageURL = list()
			self.rating = list()
			self.healthScore = list()
			self.servings = list()
			self.prepTime = list()

		def load_recipe(self, foodName = '', diet = '', intolerances = '', cuisine = '', number = ''):
			try:
				response = requests.get(f"https://api.spoonacular.com/recipes/search?query={foodName}&diet={diet}&cuisine={cuisine}&intolerances={intolerances}&number={number}&instructionsRequired=true&apiKey=1a41ac6b7c104a5cba4ce14783fdd6f7")
				response = response.json()
					
				recipe_list = []

				for num in range(int(number)):
					self.rid.append(int(response['results'][num]['id'])) # appends recipe id
					recipe_list.append(int(response['results'][num]['id'])) # create list of id's to return
					self.recipeTitle.append(str(response['results'][num]['title'])) # appends recipe title
					self.recipeInstructions.append(self.getRecipeInstructions(response['results'][num]['id'])) # append recipe instructions
					self.loadInformation(response['results'][num]['id'])
					self.loadEquipment(response['results'][num]['id'])
			except Exception as ex:
				print("failed in load_recipe")
				recipe_list.append(ex)

			return recipe_list
				
		def	getRecipeInstructions(self, id):
			response = requests.get(f"https://api.spoonacular.com/recipes/{id}/analyzedInstructions?apiKey=1a41ac6b7c104a5cba4ce14783fdd6f7")

			response = response.json()	
			instructions = list()

			for item in response[0]['steps']:
				instructions.append(item['step'])

			return instructions

		def loadInformation(self, id):
			response = requests.get(f"https://api.spoonacular.com/recipes/{id}/information?includeNutrition=false&apiKey=1a41ac6b7c104a5cba4ce14783fdd6f7")
			response = response.json()

			self.rating.append(response['spoonacularScore'])
			self.healthScore.append(response['healthScore'])
			self.servings.append(response['servings'])
			self.prepTime.append(response['readyInMinutes'])
			ingredients = list()
			for item in response['extendedIngredients']: # gets recipe ingredients
				ingredients.append(item['original'])
			self.recipeIngredients.append(ingredients)
			self.imageURL.append(response['image'])

		def loadEquipment(self, id):
			response = requests.get(f"https://api.spoonacular.com/recipes/{id}/equipmentWidget.json?&apiKey=1a41ac6b7c104a5cba4ce14783fdd6f7")

			response = response.json()
			equipment = list()

			for item in response['equipment']:
				equipment.append(item['name'])

			self.equipment.append(equipment)

		def getRecipe(self, recipe_id):
			recipe = {}
			for index, item in enumerate(self.rid):
				if int(recipe_id) == int(item):
					recipe['rid'] = item
					recipe['recipeTitle'] = self.recipeTitle[index]
					recipe['recipeIngredients'] = self.recipeIngredients[index]
					recipe['recipeInstructions'] = self.recipeInstructions[index]
					recipe['equipment'] = self.equipment[index]
					recipe['imageURL'] = self.imageURL[index]
					recipe['rating'] = self.rating[index]
					recipe['healthScore'] = self.healthScore[index]
					recipe['servings'] = self.servings[index]
					recipe['prepTime'] = self.prepTime[index]
					break
			return json.dumps(recipe)



if __name__ == '__main__':
	rdb = _recipe_database()

	foodName = "chicken"
	number = 1
	diet = ''
	intolerances = ''
	cuisine = ''
	rdb.load_recipe(foodName, diet, intolerances, cuisine, number)

	print(rdb.recipeTitle)
	print(rdb.recipeInstructions)
	print(rdb.servings)
	print(rdb.prepTime)
	print(rdb.equipment)
	print(rdb.getRecipe(rdb.rid[0]))
