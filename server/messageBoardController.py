import cherrypy
import json
from messageboard_library import _message_board

class MessageBoardController():
	def __init__(self):
		self.recipe = _message_board()

	def POST_RECIPE_BOARD(self):
		'''add recipe to message board'''
		output = {"result" : "success"}

		data = json.loads(cherrypy.request.body.read().decode('utf-8'))

		try:
			self.recipe.add_recipe(data['title'], data['ingredients'], data['instructions'], data['url'])
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = ex

		return json.dumps(output)
