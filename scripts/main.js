console.log("Page Loaded")

var searchButton = document.getElementById('search-button');
searchButton.onmouseup = getFormInfo;

var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearScreen;

function clearScreen() {
  document.getElementById("response-div").innerHTML = "";
}

function getFormInfo() {
  console.log("Retrieving Form Info")

  form_dict = {}

  // get form input
  var foodName  = document.getElementById('foodName').value;
  var cuisine   = document.getElementById('cuisine').value;
  var diet      = document.getElementById('diet').value;
  var intolerances  = document.getElementById('intolerances').value;
  var number = document.getElementById('numRecipe').value;

  form_dict['foodName'] = foodName;
  form_dict['cuisine']  = cuisine;
  form_dict['diet']     = diet;
  form_dict['intolerances'] = intolerances;
  form_dict['number']= number;

  console.log(form_dict);

  serverRequestPOST(form_dict);
}

function serverRequestPOST(form_dict) {
  console.log("In serverRequestPOST");

  var xhr = new XMLHttpRequest();
  var url = "http://localhost:51054/recipe/";

  console.log("URL: " + url);
  xhr.open("POST", url, true);

  xhr.onload = function(e) {
		var response = JSON.parse(xhr.responseText);
		console.log("Response from POST: " + response)

		for (i = 0; i < response['id'].length; i++) {
    	serverRequestGET(response['id'][i]);
		}
	}

  xhr.onerror = function(e) {
    console.error(xhr.statusText);
  }

  xhr.send(JSON.stringify(form_dict));

}

function serverRequestGET(id) {
	console.log("In serverRequestGET");
	console.log("ID: " + id);

  var xhr = new XMLHttpRequest();
  var url = "http://localhost:51054/recipe/" + id;

  console.log("URL: " + url);
  xhr.open("GET", url, true);

	xhr.onload = function(e) {
		var response = JSON.parse(xhr.responseText);
    console.log(response);
		display(response);
	}

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}

	xhr.send(null);
}

function display(response) {
	para_item = document.createElement("p");
	para_item.setAttribute("id", "dynamic-para");

	var title_node = document.createTextNode("Title: " + response['recipe']['recipeTitle'])
	para_item.appendChild(title_node);

  para_item.appendChild(document.createElement("br"));

  // dispaly health score
	var health_node = document.createTextNode("Health Score: " + response['recipe']['healthScore']);
	para_item.appendChild(health_node);

  para_item.appendChild(document.createElement("br"));

  // display rating
  var rating_node = document.createTextNode("Rating: " + response['recipe']['rating'])
  para_item.appendChild(rating_node);

  para_item.appendChild(document.createElement("br"));

  // display servings
  var servings_node = document.createTextNode("Servings: " + response['recipe']['servings']);
  para_item.appendChild(servings_node);

  para_item.appendChild(document.createElement("br"));

  // display prep time
  var prep_node = document.createTextNode("Preparation Time: " + response['recipe']['prepTime'])
  para_item.appendChild(prep_node);

  para_item.appendChild(document.createElement("br"));

  // display health score
  var health_node = document.createTextNode("Health Score: " + response['recipe']['healthScore']);
  para_item.appendChild(health_node);

  para_item.appendChild(document.createElement("br"));

  // display recipe ingredients
  para_item.appendChild(document.createTextNode("Ingredients: "));
  para_item.appendChild(document.createElement("br"));

  for (i = 0; i < response['recipe']['recipeIngredients'].length; i++) {
    para_item.appendChild(document.createTextNode("\t" + response['recipe']['recipeIngredients'][i]));
    para_item.appendChild(document.createElement("br"));
  }

  para_item.appendChild(document.createElement("br"));

  // display instructions
  para_item.appendChild(document.createTextNode("Instructions: "));
  para_item.appendChild(document.createElement("br"));

  for (i = 0; i < response['recipe']['recipeInstructions'].length; i++) {
    para_item.appendChild(document.createTextNode("\t" + response['recipe']['recipeInstructions'][i]));
    para_item.appendChild(document.createElement("br"));
  }

  para_item.appendChild(document.createElement("br"));

  // add image
  image_item = document.createElement("img");
  image_item.src = response['recipe']['imageURL'];
  para_item.appendChild(image_item);

  // add to end of html doc
  var htmlResp = document.getElementById("response-div");
  htmlResp.prepend(para_item);
}
