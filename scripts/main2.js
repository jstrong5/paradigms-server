console.log("Page Loaded")

var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = addRecipe;

function addRecipe() {
  console.log("Entered Message Board");
  var recipe = document.getElementById('text').value;

  // add to paragraph
  para_item = document.createElement("p");
	para_item.setAttribute("id", "dynamic-para");

  var recipe_node = document.createTextNode(recipe);

  para_item.appendChild(recipe_node);

  var htmlResp = document.getElementById("response-div2");
  htmlResp.prepend(para_item);

  // http request
  var xhr = new XMLHttpRequest();
  var url = "http://localhost:51054/board/";

  console.log("URL: " + url);
  xhr.open("POST", url, true);

  xhr.onload = function(e) {
    var response = JSON.parse(xhr.responseText);
    console.log(response);
    //displayMessage(response);
  }

  xhr.onerror = function(e) {
    console.error(xhr.statusText);
  }

  xhr.send(JSON.stringify(recipe));
}
