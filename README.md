# Paradigms-Server
# jstrong5
# abisigna

JSON Specification

GET
/recipe/
There should be no body to a GET request
string formatted json of recipe information
GET_RECIPE
Grabs specs for a recipe based on user input

GET
/recipe/:recipe_id
There should be no body to a GET request
string formatted json of recipe instructions/ingredients/equipment/health score/servings
GET_RECIPE_INSTRUCTIONS
Gets recipe instructions/ingredients/equipment/health score/servings

PUT
/recipe/board/:recipe_id
Body will be a json format of recipe from user
{“result”:”success”} if the operation worked
PUT_RECIPE
Changes recipe of board

POST
/recipe/board
Body will be a json format of a recipe the user might input, assigns an id to the recipe
{“result”:”success”} if the operation worked
POST_RECIPE_BOARD
Posts recipe to message board


#Testing
We searched for numerous inputs and made sure that everything worked in our webclient.
We also built a test_server.py program that makes requests to the server and makes sure that the controller returns the correct output.


